#include <iostream>

using namespace std;

int main()
{
    int n, m;

    cout << "n: ";
    cin >> n;
    cout << "m: ";
    cin >> m;

    for(int i = 1; i<=n; i++)
    {
        for(int j = 1; j<=m; j++)
        {
            cout << i * j << " ";
        }
        cout << "\n";
    }

    return 0;
}
