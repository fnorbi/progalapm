#include <iostream>

using namespace std;

int main()
{
    //Deklaracio
    double a, b, c;
    double maximum;

    //Beolvasas
    cout << "a: ";
    cin >> a;
    cout << "b: ";
    cin >> b;
    cout << "c: ";
    cin >> c;

    //Feldolgozas1
    /*
    if(a >= b && a >= c)
    {
        maximum = a;
    }
    else if(b >= c)
    {
        maximum = b;
    }
    else
    {
        maximum = c;
    }
    */

    maximum = a;
    if(b > maximum) maximum = b;
    if(c > maximum) maximum = c;

    //Kiiras
    cout << "A megadott szamok maximuma: " << maximum << endl;

    return 0;
}
