#include <iostream>
#include <vector>

using namespace std;

typedef vector<vector<int>> Matrix;

void beolvas(int &n, Matrix &napok)
{
    cin >> n;
    napok.resize(n);
    for(int i = 0; i < n; i++)
    {
        napok[i].resize(7);
        for(int j = 0; j < 7; j++)
        {
            cin >> napok[i][j];
        }
    }
    /*
    for(int i = 0; i < n; i++)
    {
        napok[i].resize(7);
        for(int j = 0; j < 7; j++)
        {
            cout << napok[i][j] << " ";
        }
        cout << endl;
    }
    */
}

int osszeg(vector<int> t)
{
    int s = 0;
    for(int i = 0; i < t.size(); i++)
    {
        s += t[i];
    }
    return s;
}

void feladat1(Matrix napok, vector<int> &hetek)
{
    int n = napok.size();

    for(int i = 0; i < n; i++)
    {
        hetek[i] = osszeg(napok[i]);
    }

    for(int i = 0; i < n; i++)
    {
        cout << hetek[i] << " ";
    }
    cout << endl;
}

void feladat2(vector<int> hetek)
{
    int maxi = 0;
    for(int i = 1; i < hetek.size(); i++)
    {
        if(hetek[i] > hetek[maxi])
        {
            maxi = i;
        }
    }

    cout << maxi+1 << endl;
}

bool novekvo(vector<int> v)
{
    int i = 1;
    while(i < v.size() && v[i-1] < v[i])
    {
        i++;
    }
    return i >= v.size();
}

void feladat3(Matrix napok)
{
    int n = napok.size();
    vector<int> novekvoek;
    for(int i = 0; i < n; i++)
    {
        if(novekvo(napok[i]))
        {
            novekvoek.push_back(i);
        }
    }

    cout << novekvoek.size() << " ";
    for(int i = 0; i < novekvoek.size(); i++)
    {
        cout << novekvoek[i]+1 << " ";
    }
    cout << endl;
}

//t vektornak hany darab 0 eleme van
int sordarab(vector<int> t)
{
    int db = 0;
    for(int i = 0; i < t.size(); i++)
    {
        if(t[i] == 0)
        {
            db++;
        }
    }
    return db;
}

//napok matrixban a+1. sortol b+1. sorig hany 0 ertek van
int darab(Matrix napok, int a, int b)
{
    int db = 0;
    for(int i = a; i <= b; i++)
    {
        db += sordarab(napok[i]);
    }
    return db;
}

void feladat4(Matrix napok)
{
    //Legkevesebb eso = legtobb 0 ertek
    int n = napok.size();
    int maxi = 0;
    int maxert = darab(napok, 0, 0+n/2-1); //elso n/2 sorban hany 0 ertek van
    for(int eleje = 1; eleje < n - n/2 + 1; eleje++)
    {
        int vege = eleje + n/2 - 1;
        int akt = darab(napok, eleje, vege);
        if(akt > maxert)
        {
            maxert = akt;
            maxi = eleje;
        }
    }
    cout << maxi+1 << endl;
}

int main()
{
    int n;
    Matrix napok;

    beolvas(n, napok);

    vector<int> hetek(n); //hetek[i]: i+1. heten mennyi eso esett
    feladat1(napok, hetek);
    feladat2(hetek);
    feladat3(napok);
    feladat4(napok);
    cout << "\n";

    return 0;
}
