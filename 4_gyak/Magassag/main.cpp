#include <iostream>
#include <vector>

using namespace std;

struct Tanulo
{
    string nev;
    int mag;
};

int main()
{
    int n;
    vector<Tanulo> tanulok;
    bool azonosE;

    cin >> n;
    tanulok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> tanulok[i].nev >> tanulok[i].mag;
    }

    int i;
    i = 0;
    while(i<n-1 && tanulok[i].mag <= tanulok[i+1].mag)
    {
        i++;
    }

    azonosE = i>=n-1;

    if(azonosE)
    {
        cout << "Azonos!\n";
    }
    else
    {
        cout << "Nem azonos!\n";
    }

    return 0;
}
