#include <iostream>
#include <vector>

using namespace std;

//Olvassunk be osztalyzatokat es allapitsuk meg, hogy
//osszessegeben hany darab szuletett az egyes jegyekbol.

int main()
{
    int n;
    vector<int> jegyek;

    //Beolvasas

    cin >> n;
    jegyek.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> jegyek[i];
    }

    //Feldolgozas
    //szamlaloTomb[i]: i+1. jegybol hany darab lett
    int szamlaloTomb[5];
    for(int i = 0; i < 5; i++)
    {
        szamlaloTomb[i] = 0;
    }
    for(int i = 0; i < n; i++)
    {
        int jegy = jegyek[i];
        szamlaloTomb[jegy-1]++;
    }

    //Kiir
    for(int i = 0; i < 5; i++)
    {
        cout << szamlaloTomb[i] << " ";
    }

    return 0;
}
