#include <iostream>
#include <vector>

using namespace std;

struct Ember
{
    string nev;
    char nem;
};

int main()
{
    int n;
    vector<Ember> emberek;
    int ind;
    string nev;

    cin >> n;
    emberek.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> emberek[i].nev >> emberek[i].nem;
    }

    int i;
    i = 0;
    while(emberek[i].nem != 'n')
    {
        i++;
    }

    ind = i;
    nev = emberek[i].nev;

    cout << "Sorszama: " << ind+1 << endl;
    cout << "Neve: " << nev << endl;

    return 0;
}
