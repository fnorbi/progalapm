#include <iostream>

using namespace std;

const string napok[7] = {"hetfo", "kedd", "szerda", "csutortok", "pentek", "szombat", "vasarnap"};

int main()
{
    int n;
    string s;

    cout << "n: ";
    cin >> n;

    /*
    if(n == 1)
    {
        s = "hetfo";
    }
    else if(n == 2)
    {
        s = "kedd";
    }
    //stb...
    */

    /*
    switch(n)
    {
    case 1:
        s = "hetfo";
        break;
    case 2:
        s = "kedd";
        break;
    case 3:
        s = "szerda";
        break;
    case 4:
        s = "csutortok";
        break;
    case 5:
        s = "pentek";
        break;
    case 6:
        s = "szombat";
        break;
    case 7:
        s = "vasarnap";
        break;
    default:
        s = "Nem jo szamot adtal meg!";
        break;
    }
    */

    if(1 <= n && n <= 7)
    {
        s = napok[n-1];
        cout << s << endl;
    }

    return 0;
}
