#include <cmath>

struct Komplex
{
    double a, b;

    Komplex(double a, double b);
    double abs();
};
