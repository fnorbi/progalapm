#include <iostream>
#include <cmath>

using namespace std;

struct Komplex
{
    double a, b;
};

int main()
{
    Komplex z;
    z.a = 1;
    z.b = 1;
    cout << sqrt(z.a*z.a + z.b*z.b) << endl;

    return 0;
}
