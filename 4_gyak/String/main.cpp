#include <iostream>

using namespace std;

int main()
{
    string s;

    cin >> s;

    //string = karakterek tombje
    //s[i]: a string i+1. sorszamu karaktere
    //s.length(): hany karakter hosszu a string
    cout << s << " " << s[0] << " " << s.length() << endl;

    return 0;
}
