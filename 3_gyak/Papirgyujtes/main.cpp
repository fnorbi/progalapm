#include <iostream>
#include <vector>

using namespace std;

int main()
{
    // Deklaracio
    int n;
    vector<int> tomegek;

    // Beolvasas (baratsagos es biztonsagos)
    bool jo;
    do
    {
        cout << "Add meg hany csomagot hoztak: ";
        cin >> n;
        jo = n > 0;
        if(!jo)
        {
            cout << "Nem jo: n > 0 kellene!\n";
        }
    }while(!jo);

    tomegek.resize(n);
    for(int i = 0; i < n; i++)
    {
        /*
        int a;
        cin >> a;
        tomegek.push_back(a);
        */
        do
        {
            cout << "Add meg a(z) " << i+1 << ". tomeget: ";
            cin >> tomegek[i];
            jo = 0 < tomegek[i] && tomegek[i] <= 500;
            if(!jo)
            {
                cout << "Nem jo! (0 < tomegek[i] <= 500)\n";
            }
        }while(!jo);
    }

    // Feldolgozas
    int osszeg;
    int szOsszeg, nDarab;
    double atlag;

    osszeg = 0;
    for(int i = 0; i < n; i++)
    {
        osszeg = osszeg + tomegek[i];
    }
    szOsszeg = 0; nDarab = 0;
    for(int i = 0; i < n; i++)
    {
        if(tomegek[i] <= 100)
        {
            szOsszeg += tomegek[i];
            nDarab++;
        }
    }
    atlag = (double)szOsszeg / nDarab;

    // Kiiras
    cout << "A tomegek osszege: " << osszeg << endl;
    cout << "A legfeljebb 100 kg-ot hozottak atlaga: " << atlag << endl;

    return 0;
}
