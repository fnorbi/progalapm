#include <iostream>
#include <vector>

using namespace std;

const int MAXN = 8;

int main()
{
    // 2 elemu tombok deklaracioja, a jelentese: az a tomb elso elemenek memoriacime
    int a[2] = {1, 2};
    int b[2] = {3, 4};

    // *(a+i) a tomb (i+1). eleme, egyszerubb jelolessel: a[i]
    //cout << *a << " " << *(a+1) << endl;
    //cout << a[0] << " " << a[1];
    //cout << a[2];
    
    // b-a jelentese: b es a memoria cimek (szamok) kulonbsege
    //cout << *(a + (b - a)) << " " << a[b-a];

    // Vigyazat! A kiindexeles veszelyes!
    // Itt b valtozon keresztul megvaltoztatjuk a tomb elso elemenek erteket!
    //cout << a[0] << endl;
    //b[2] = 73;
    //cout << a[0] << endl;

    // Statikus tombok => meretet forditasi idoben ismerni kell
    /*
    int t[MAXN];
    for(int i = 0; i < MAXN; i++)
    {
        cin >> t[i];
    }
    for(int i = 0; i < MAXN; i++)
    {
        cout << t[i] << " ";
    }
    */

    // Vektor: dinamikus tomb, valtoztathato a merete futas kozben is
    // v.size(): v vektor elemeinek szama
    // v.resize(n): v vektor elemeinek szama legyen n (itt lehet n valtozo is, ami csak futas kozben kap erteket)
    // v.push_back(a): noveljuk meg v jelenlegi meretet 1-gyel es utolso eleme legyen a ertek
    /*
    vector<int> v;
    cout << v.size() << " ";
    v.push_back(3);
    cout << v.size() << " " << v[0] << " ";
    v.resize(5);
    cout << v.size() << endl;
    */

    vector<int> v;
    int n;
    do
    {
        cin >> n;
        v.push_back(n);
    }while(n != 5);

    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }

    return 0;
}
