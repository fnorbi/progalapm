#include <iostream>
#include <vector>

using namespace std;

struct Tanulo
{
    string nev;
    int pont;
};

void beolvas(int &n, int &m, vector<Tanulo> &zh, vector<Tanulo> &potzh)
{
    cin >> n;
    zh.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> zh[i].nev >> zh[i].pont;
    }
    cin >> m;
    potzh.resize(m);
    for(int i = 0; i < m; i++)
    {
        cin >> potzh[i].nev >> potzh[i].pont;
    }
}

bool eleme(Tanulo tanulo, vector<Tanulo> tanulok)
{
    int i = 0;
    while(i<tanulok.size() && tanulo.nev != tanulok[i].nev)
    {
        i++;
    }
    return i<tanulok.size();
}

void mindketton(vector<Tanulo> zh, vector<Tanulo> potzh, int &db, vector<Tanulo> &mindketto)
{
    db = 0;
    for(int i = 0; i < zh.size(); i++)
    {
        if(eleme(zh[i], potzh))
        {
            db++;
            mindketto.push_back(zh[i]);
        }
    }
}

void kiir(vector<Tanulo> tanulok)
{
    cout << tanulok.size() << endl;
    for(int i = 0; i < tanulok.size(); i++)
    {
        cout << tanulok[i].nev << endl;
    }
}

int main()
{
    int n, m;
    vector<Tanulo> zh, potzh;
    int db;
    vector<Tanulo> mindketto;

    beolvas(n, m, zh, potzh);
    mindketton(zh, potzh, db, mindketto);
    kiir(mindketto);

    return 0;
}
