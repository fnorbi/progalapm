#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, vector<int> &szamok)
{
    bool jo;
    do
    {
        //cout << "Hany szamot huztak ki (n>=0): ";
        cin >> n;
        jo = n>=0;
        if(!jo)
        {
            cout << "Nem jo! (n>=0)" << endl;
        }
    }while(!jo);

    szamok.resize(n);
    for(int i = 0; i < n; i++)
    {
        do
        {
            //cout << "A(z) " << i+1 << ". szam [1..100]: ";
            cin >> szamok[i];
            jo = szamok[i]>=1 && szamok[i]<=100;
            if(!jo)
            {
                cout << "Nem jo! A szam 1 es 100 kozotti kell legyen." << endl;
            }
        }while(!jo);
    }
}

bool eleme(int elem, int db, vector<int> h)
{
    int i = 0;
    while(i<db && h[i] != elem)
    {
        i++;
    }
    return i<db;
}

void halmazkeszites(int n, vector<int> szamok, int &db, vector<int> &h)
{
    db = 0;
    for(int i = 0; i < n; i++)
    {
        if(!eleme(szamok[i], db, h))
        {
            db++;
            h.push_back(szamok[i]);
        }
    }
}

void kiir(int db, vector<int> h)
{
    if(db == 0)
    {
        cout << "Nem huztunk egyetlen szamot sem!" << endl;
    }
    else
    {
        cout << "A halmaz elemszama: " << db << endl;
        cout << "Az elemek: " << endl;
        for(int i = 0; i < db; i++)
        {
            cout << h[i] << " ";
        }
    }
}

int main()
{
    int n;
    vector<int> szamok;
    int db;
    vector<int> h;

    beolvas(n, szamok);
    halmazkeszites(n, szamok, db, h);
    kiir(db, h);

    return 0;
}
