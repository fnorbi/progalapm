#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, int &m, vector<vector<int>> &h)
{
    int k;
    cin >> n >> m >> k;
    h.resize(n);
    for(int i = 0; i < n; i++)
    {
        h[i].resize(m);
        for(int j = 0; j < m; j++)
        {
            cin >> h[i][j];
        }
    }
}

int osszeg(vector<int> v)
{
    int s = 0;
    for(int i = 0; i < v.size(); i++)
    {
        s += v[i];
    }
    return s;
}

void legnagyobbAtlag(vector<vector<int>> h, int &maxi, int &maxe)
{
    maxi = 0;
    maxe = osszeg(h[0]);
    for(int i = 1; i < h.size(); i++)
    {
        int aktOsszeg = osszeg(h[i]);
        if(aktOsszeg > maxe)
        {
            maxi = i;
            maxe = aktOsszeg;
        }
    }
}

void kiir(int maxi)
{
    cout << maxi+1 << endl;
}

int main()
{
    /*
    int t[4] = {1, 2, 3, 4};
    int m[2][3] = {{1, 2, 3},
                   {4, 5, 6}};

    cout << m[1][2];
    */

    int n, m;
    vector<vector<int>> h;
    int maxi, maxe;

    beolvas(n, m, h);
    legnagyobbAtlag(h, maxi, maxe);
    kiir(maxi);

    return 0;
}
