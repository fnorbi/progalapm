#include <iostream>
#include <vector>

using namespace std;

typedef vector<vector<bool>> Matrix;

struct Nevezes
{
    int tanulo; // sorszam
    int verseny;
};

int kereses(string nev, vector<string> nevek)
{
    int i = 0;
    while(i < nevek.size() && nev != nevek[i])
    {
        i++;
    }
    return i;
}

void beolvas(int &s, int &n, vector<Nevezes> &nevezesek, vector<string> &nevek)
{
    cin >> s >> n;
    nevezesek.resize(n);
    for(int i = 0; i < n; i++)
    {
        string nev;
        cin >> nev >> nevezesek[i].verseny;
        int sorsz = kereses(nev, nevek);
        if(sorsz == nevek.size())
        {
            nevek.push_back(nev);
        }
        nevezesek[i].tanulo = sorsz+1;
    }

    /*
    for(int i = 0; i < n; i++)
    {
        int sorsz = nevezesek[i].tanulo;
        cout << sorsz << " " << nevek[sorsz-1] << " " << nevezesek[i].verseny << endl;
    }
    */
}

void szamlal(vector<Nevezes> nevezesek, vector<int> &hanyNevezes)
{
    for(int i = 0; i < nevezesek.size(); i++)
    {
        int tanulo = nevezesek[i].tanulo;
        hanyNevezes[tanulo-1]++;
    }

    /*
    for(int i = 0; i < hanyNevezes.size(); i++)
    {
        cout << hanyNevezes[i] << " ";
    }
    cout << endl;
    */
}

void feladat1(vector<int> hanyNevezes, vector<string> nevek)
{
    cout << "#" << endl;
    int i = 0;
    while(i < hanyNevezes.size() && hanyNevezes[i] != 1)
    {
        i++;
    }
    if(i < hanyNevezes.size())
    {
        cout << nevek[i] << endl;
    }
    else
    {
        cout << endl;
    }
}

void feladat2(vector<int> hanyNevezes, vector<string> nevek)
{
    cout << "#" << endl;
    int maxi = 0;
    for(int i = 1; i < hanyNevezes.size(); i++)
    {
        if(hanyNevezes[i] > hanyNevezes[maxi])
        {
            maxi = i;
        }
    }
    cout << nevek[maxi] << endl;
}

void feladat3(vector<Nevezes> nevezesek, vector<int> &hanyNevezo)
{
    cout << "#" << endl;
    for(int i = 0; i < nevezesek.size(); i++)
    {
        int verseny = nevezesek[i].verseny;
        hanyNevezo[verseny-1]++;
    }

    for(int i = 0; i < hanyNevezo.size(); i++)
    {
        cout << hanyNevezo[i] << " ";
    }
    cout << endl;
}

void feltolt(Matrix &nevezett, int tsz, int s, vector<Nevezes> nevezesek)
{
    nevezett.resize(tsz);
    for(int i = 0; i < tsz; i++)
    {
        nevezett[i].resize(s);
        for(int j = 0; j < s; j++)
        {
            nevezett[i][j] = false;
        }
    }

    for(int i = 0; i < nevezesek.size(); i++)
    {
        int tanulo = nevezesek[i].tanulo;
        int verseny = nevezesek[i].verseny;
        nevezett[tanulo-1][verseny-1] = true;
    }

    /*
    for(int i = 0; i < tsz; i++)
    {
        for(int j = 0; j < s; j++)
        {
            cout << nevezett[i][j] << " ";
        }
        cout << endl;
    }
    */
}

// jopar i es j sportag ha nincs olyan k tanulo, aki mindkettore nevezett (ekkor rendezheto egyszerre)
bool jopar(int i, int j, Matrix nevezett)
{
    int k = 0; // k most tanulot jelol
    while(k < nevezett.size() && !(nevezett[k][i] && nevezett[k][j]))
    {
        k++;
    }
    return k >= nevezett.size();
}

void feladat4(Matrix nevezett, vector<int> hanyNevezo)
{
    cout << "#" << endl;
    int s = nevezett[0].size(); // sportagak szama
    for(int i = 0; i < s-1; i++)
    {
        for(int j = i+1; j < s; j++)
        {
            if(hanyNevezo[i]>0 && hanyNevezo[j]>0 && jopar(i,j,nevezett))
            {
                cout << i+1 << " " << j+1 << endl;
            }
        }
    }
}

// talalkozhatnak i es j tanulok ha van olyan k verseny, amire mindketten neveztek
bool talalkozhat(int i, int j, Matrix nevezett)
{
    int k = 0; // k most versenyt jelol
    while(k < nevezett[0].size() && !(nevezett[i][k] && nevezett[j][k]))
    {
        k++;
    }
    return k < nevezett[0].size();
}

void feladat5(Matrix nevezett, vector<int> &talalkozasok, vector<string> nevek)
{
    cout << "#" << endl;
    int tsz = nevezett.size(); // tanulok szama
    for(int i = 0; i < tsz-1; i++)
    {
        for(int j = i+1; j < tsz; j++)
        {
            if(talalkozhat(i,j,nevezett))
            {
                talalkozasok[i]++;
                talalkozasok[j]++;
            }
        }
    }

    for(int i = 0; i < nevezett.size(); i++)
    {
        cout << nevek[i] << " " << talalkozasok[i] << endl;
    }
}

int main()
{
    int s, n; // s = sportagak szama, n = nevezesek szama
    vector<Nevezes> nevezesek;
    vector<string> nevek; // nevek[i]: i+1. tanulo nevet

    beolvas(s, n, nevezesek, nevek);
    int tsz = nevek.size();
    vector<int> hanyNevezes(tsz, 0); // hanyNevezes[i]: i+1. tanulo hany versenyre nevezett
    szamlal(nevezesek, hanyNevezes);
    feladat1(hanyNevezes, nevek);
    feladat2(hanyNevezes, nevek);

    vector<int> hanyNevezo(s, 0); // hanyNevezo[i]: i+1. sportagra hany versenyzo nevezett
    feladat3(nevezesek, hanyNevezo);

    Matrix nevezett; // nevezett[i][j]: i+1. tanulo nevezett-e a j+1. sportagra?
    feltolt(nevezett, tsz, s, nevezesek);
    feladat4(nevezett, hanyNevezo);

    vector<int> talalkozasok(tsz, 0); // talalkozasok[i]: i+1. tanulo hany masikkal talalkozhat
    feladat5(nevezett, talalkozasok, nevek);

    return 0;
}
