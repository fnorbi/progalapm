#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, int &t, vector<int> &tornadok)
{
    cin >> n >> t;
    tornadok.resize(t);
    for(int i = 0; i < t; i++)
    {
        cin >> tornadok[i];
    }
}

void szamlalas(vector<int> tornadok, vector<int> &napok)
{
    for(int i = 0; i < napok.size(); i++)
    {
        napok[i] = 0;
    }
    for(int i = 0; i < tornadok.size(); i++)
    {
        int nap = tornadok[i];
        napok[nap-1]++;
    }
}

void feladat1(vector<int> napok)
{
    cout << "#" << endl;
    int db = 0;
    for(int i = 0; i < napok.size(); i++)
    {
        if(napok[i] == 0)
        {
            db++;
        }
    }
    cout << db << endl;
}

void feladat2(vector<int> napok)
{
    cout << "#" << endl;
    int i = 1;
    while(i < napok.size()-1 && !(napok[i] == 1 && napok[i-1] == 1 && napok[i+1] == 1))
    {
        i++;
    }
    if(i < napok.size()-1)
    {
        cout << i+1 << endl;
    }
    else
    {
        cout << 0 << endl;
    }
}

void feladat3(int n, vector<int> tornadok)
{
    cout << "#" << endl;
    int maxe = tornadok[0]-1;
    for(int i = 1; i < tornadok.size(); i++)
    {
        if(tornadok[i]-tornadok[i-1]-1 > maxe)
        {
            maxe = tornadok[i]-tornadok[i-1]-1;
        }
    }
    if(n - tornadok[tornadok.size()-1] > maxe)
    {
        maxe = n - tornadok[tornadok.size()-1];
    }
    cout << maxe << endl;
}

void feladat4(vector<int> napok)
{
    cout << "#" << endl;
    int maxe = napok[0];
    for(int i = 1; i < napok.size(); i++)
    {
        if(napok[i] > maxe)
        {
            maxe = napok[i];
        }
    }
    cout << maxe << endl;
}

int szakaszveg(int akt, vector<int> napok)
{
    int j = akt+1;
    while(j < napok.size() && napok[j] > 0)
    {
        j++;
    }
    return j-1;
}

void feladat5(vector<int> napok)
{
    cout << "#" << endl;
    int k, v;
    int maxe = -1; // a leghosszabb szakasz hossza
    int i = 0;
    while(i < napok.size())
    {
        if(napok[i] > 0)
        {
            int j = szakaszveg(i, napok);
            if(j-i > maxe)
            {
                maxe = j-i;
                k = i;
                v = j;
            }
        }
        i++;
    }
    cout << k+1 << " " << v+1 << endl;
}

int main()
{
    int n, t;
    vector<int> tornadok;

    beolvas(n, t, tornadok);

    vector<int> napok(n); //napok[i]: i+1. napon hany tornado volt
    szamlalas(tornadok, napok);
    feladat1(napok);
    feladat2(napok);
    feladat3(n, tornadok);
    feladat4(napok);
    feladat5(napok);

    return 0;
}
