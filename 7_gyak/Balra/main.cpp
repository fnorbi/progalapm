#include <iostream>
#include <vector>

using namespace std;

//const int MAXN = 100;

/*
void beolvas(int &n, int t[MAXN])
{
    cin >> n;
    for(int i = 0; i < n; i++)
    {
        cin >> t[i];
    }
}
*/

void beolvas(int &n, vector<int> &t)
{
    cin >> n;
    t.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> t[i];
    }
}

void balratol(const int n, const vector<int> t, vector<int> &eltolt)
{
    eltolt.resize(n);
    for(int i = 0; i < n-1; i++)
    {
        eltolt[i] = t[i+1];
    }
    eltolt[n-1] = t[0];
}

void kiir(const int n, const vector<int> eltolt)
{
    for(int i = 0; i < n; i++)
    {
        cout << eltolt[i] << " ";
    }
}

int main()
{
    int n;
    //int t[MAXN];
    //int eltolt[MAXN];
    vector<int> t;
    vector<int> eltolt;

    beolvas(n, t);
    balratol(n, t, eltolt);
    kiir(n, eltolt);

    return 0;
}
