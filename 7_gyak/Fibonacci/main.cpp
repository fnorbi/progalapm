#include <iostream>
#include <vector>
#include <limits.h>
#include <cmath>

using namespace std;

int fib(const int n)
{
    if(n == 1 || n == 2)
    {
        return 1;
    }
    else
    {
        return fib(n-1) + fib(n-2);
    }
}

long long fib2(const int n)
{
    vector<long long> f(n);
    f[0] = 1;
    f[1] = 1;
    for(int i = 2; i < n; i++)
    {
        f[i] = f[i-1] + f[i-2];
    }
    return f[n-1];
}

long long fib3(const int n)
{
    return 1/sqrt(5) * ( pow((1+sqrt(5))/2, n) - pow((1-sqrt(5))/2, n) );
}

int main()
{
    int n;
    long long fibonacci;

    cin >> n;
    fibonacci = fib3(n);
    cout << fibonacci;

    return 0;
}
