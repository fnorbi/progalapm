#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, vector<int> &u)
{
    cin >> n;
    u.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> u[i];
    }
}

int f(const int u)
{
    if(u>=0)
    {
        return u;
    }
    else
    {
        return 0;
    }
}

void fuggvenyszamitas(const int n, const vector<int> u, vector<int> &v)
{
    v.resize(n);
    for(int i = 0; i < n; i++)
    {
        v[i] = f(u[i]);
    }
}

void kiir(const int n, const vector<int> v)
{
    for(int i = 0; i < n; i++)
    {
        cout << v[i] << " ";
    }
}

int main()
{
    int n;
    vector<int> u, v;

    beolvas(n, u);
    fuggvenyszamitas(n, u, v);
    kiir(n, v);

    return 0;
}
