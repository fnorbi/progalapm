#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, vector<int> &h)
{
    cin >> n;
    h.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> h[i];
    }
}

/*
void szetvalogat(int n, vector<int> h, int &dba, int &dbn, int &dbf,
                 vector<int> &alattiak, vector<int> &nullak, vector<int> &felettiek)
{
    dba = 0; dbn = 0; dbf = 0;
    for(int i = 0; i < n; i++)
    {
        if(h[i] < 0)
        {
            dba++;
            alattiak.push_back(i);
        }
        else if(h[i] == 0)
        {
            dbn++;
            nullak.push_back(i);
        }
        else
        {
            dbf++;
            felettiek.push_back(i);
        }
    }
}

void kiir(int db, vector<int> v)
{
    for(int i = 0; i < db; i++)
    {
        cout << v[i]+1 << " ";
    }
    cout << endl;
}
*/

//Vektorok meretet nem muszaj atadni, hiszen a size() fuggvennyel barmikor lekerdezhetok.
//Itt igazabol n erteket is felesleges atadni: h.size() fuggvenyhivassal megkaphatjuk.
void szetvalogat(int n, vector<int> h,
                 vector<int> &alattiak, vector<int> &nullak, vector<int> &felettiek)
{
    for(int i = 0; i < n; i++)
    {
        if(h[i] < 0) alattiak.push_back(i);
        else if(h[i] == 0) nullak.push_back(i);
        else felettiek.push_back(i);
    }
}

void kiir(vector<int> v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i]+1 << " ";
    }
    cout << endl;
}

int main()
{
    int n;
    vector<int> h;
    //int dba, dbn, dbf;
    vector<int> alattiak, nullak, felettiek;

    beolvas(n, h);
    //szetvalogat(n, h, dba, dbn, dbf, alattiak, nullak, felettiek);
    //kiir(dba, alattiak);
    //kiir(dbn, nullak);
    //kiir(dbf, felettiek);

    szetvalogat(n, h, alattiak, nullak, felettiek);
    kiir(alattiak);
    kiir(nullak);
    kiir(felettiek);

    return 0;
}
