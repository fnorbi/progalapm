#include <iostream>
#include <vector>

using namespace std;

struct Vektor
{
    int x, y;
};

void beolvas(int &n, vector<Vektor> &m)
{
    cin >> n;
    m.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> m[i].x >> m[i].y;
    }
}

bool hosszu(const Vektor a)
{
    return a.x*a.x+a.y*a.y>25;
}

void kivalogat(const int n, const vector<Vektor> m, int &db, vector<int> &hosszuak)
{
    db = 0;
    //hosszuak.resize(n);
    for(int i = 0; i < n; i++)
    {
        if(hosszu(m[i]))
        {
            //hosszuak[db] = i;
            db++;
            hosszuak.push_back(i);
        }
    }
}

void kiir(const int db, const vector<int> t)
{
    cout << db << endl;
    for(int i = 0; i < db; i++)
    {
        cout << t[i]+1 << " ";
    }
}

int main()
{
    int n;
    vector<Vektor> m;
    int db;
    vector<int> hosszuak;

    beolvas(n, m);
    kivalogat(n, m, db, hosszuak);
    kiir(db, hosszuak);

    return 0;
}
