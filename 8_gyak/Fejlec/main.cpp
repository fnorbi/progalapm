#include <iostream>

using namespace std;

//Fejlec vagy prototipus
int f(int a);
int g(int a);

int f(int a)
{
    if(a == 5)
    {
        return a;
    }
    else
    {
        return g(a);
    }
}

int g(int a)
{
    return f(5);
}

int main()
{
    cout << f(3);
    return 0;
}
