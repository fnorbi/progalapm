#include <iostream>
#include <vector>

using namespace std;

struct Bor
{
    int menny;
    int ar;
};

void beolvas(int &n, vector<Bor> &borok)
{
    cin >> n;
    borok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> borok[i].menny >> borok[i].ar;
    }
    /*
    for(int i = 0; i < n; i++)
    {
        cout << borok[i].menny << " " << borok[i].ar << endl;
    }
    */
}

void feladat1(vector<Bor> borok)
{
    int mini = 0;
    for(int i = 1; i < borok.size(); i++)
    {
        if(borok[i].menny < borok[mini].menny)
        {
            mini = i;
        }
    }
    cout << mini+1 << endl;
}

void feladat2(vector<Bor> borok)
{
    int maxar = -1;
    for(int i = 0; i < borok.size(); i++)
    {
        if(borok[i].menny > 1000 && borok[i].ar > maxar)
        {
            maxar = borok[i].ar;
        }
    }
    cout << maxar << endl;
}

bool eleme(int e, vector<int> v)
{
    int i = 0;
    while(i < v.size() && e!=v[i])
    {
        i++;
    }
    return i < v.size();
}

void feladat3(vector<Bor> borok)
{
    vector<int> arak;
    for(int i = 0; i < borok.size(); i++)
    {
        if(!eleme(borok[i].ar, arak))
        {
            arak.push_back(borok[i].ar);
        }
    }
    cout << arak.size() << endl;
}

bool nagyobb(int akt, vector<Bor> borok)
{
    int i = 0;
    while(i < akt && !(borok[i].menny>=borok[akt].menny))
    {
        i++;
    }
    return i >= akt;
}

void feladat4(vector<Bor> borok)
{
    vector<int> sorsz;
    for(int i = 1; i < borok.size(); i++)
    {
        if(nagyobb(i, borok)) // ha i. indexu bor ara nagyobb mint barmelyik korabbi
        {
            sorsz.push_back(i);
        }
    }
    cout << sorsz.size() << " ";
    for(int i = 0; i < sorsz.size(); i++)
    {
        cout << sorsz[i]+1 << " ";
    }
    cout << endl;
}

int main()
{
    int n;
    vector<Bor> borok;

    beolvas(n, borok);
    feladat1(borok);
    feladat2(borok);
    feladat3(borok);
    feladat4(borok);

    return 0;
}
