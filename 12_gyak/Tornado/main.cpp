#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, int &t, vector<int> &tornadok)
{
    cin >> n >> t;
    tornadok.resize(t);
    for(int i = 0; i < t; i++)
    {
        cin >> tornadok[i];
    }
}

void szamlalas(vector<int> tornadok, vector<int> &napok)
{
    for(int i = 0; i < napok.size(); i++)
    {
        napok[i] = 0;
    }
    for(int i = 0; i < tornadok.size(); i++)
    {
        int nap = tornadok[i];
        napok[nap-1]++;
    }
}

int main()
{
    int n, t;
    vector<int> tornadok;

    beolvas(n, t, tornadok);

    vector<int> napok(n); //napok[i]: i+1. napon hany tornado volt
    szamlalas(tornadok, napok);

    for(int i = 0; i < n; i++)
    {
        cout << napok[i] << " ";
    }
    cout << endl;

    return 0;
}
