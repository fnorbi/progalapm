#include <iostream>
#include <vector>

using namespace std;

void beolvas(int &n, vector<int> &ropik)
{
    cin >> n;
    ropik.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> ropik[i];
    }
}

void csere(int &a, int &b)
{
    int seged = a;
    a = b;
    b = seged;
}

void rendez(vector<int> &ropik)
{
    int n = ropik.size();
    for(int i = 0; i < n-1; i++)
    {
        int mini = i;
        for(int j = i+1; j < n; j++)
        {
            if(ropik[j] < ropik[mini])
            {
                mini = j;
            }
        }
        csere(ropik[i], ropik[mini]);
    }
}

int main()
{
    int n;
    vector<int> ropik;

    beolvas(n, ropik);
    rendez(ropik);

    cout << ropik[n/2] << endl;

    return 0;
}
