#include <iostream>

using namespace std;

//#define BIRO

int beEgesz(string ki, int MINN, int MAXN)
{
    int n;
    bool jo;
    string hiba = "Hiba! A megadott erteknek " + to_string(MINN) + " es " + to_string(MAXN) + " kozotti egesznek kell lennie!\n";
    string tmp;
    do
    {
        cout << ki;
        cin >> n;
        jo = !cin.fail() && (cin.peek()=='\n' || cin.peek()==' ') && MINN<=n && n<=MAXN;
        if(!jo)
        {
            cout << hiba;
            cin.clear();
            getline(cin, tmp);
        }
    }while(!jo);
    return n;
}

int main()
{
    int n;
    #ifdef BIRO
        cin >> n;
    #else
        string ki = "Add meg n erteket: ";
        n = beEgesz(ki, 0, 100);
    #endif
    cout << n << endl;

    return 0;
}
