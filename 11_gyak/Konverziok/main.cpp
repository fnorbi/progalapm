#include <iostream>

using namespace std;

int main()
{
    cout << (double)2/3 << endl;
    cout << (int)1.5 << endl;
    cout << (int)'A' << endl; //ASCII tabla
    cout << (char)60 << endl;
    cout << to_string(1) + to_string(1) << endl;
    cout << stoi("123456") + 1 << endl; //String TO Integer

    return 0;
}
