#include <iostream>
#include <vector>

using namespace std;

struct Benzinkut
{
    int tav;
    int menny;
};

int main()
{
    int kilometer;
    int tankolasokSzama;
    int benzin;
    int fogyasztas;
    vector<Benzinkut> tankolasok;
    bool van;
    int ind;

    cin >> kilometer >> tankolasokSzama >> benzin >> fogyasztas;
    tankolasok.resize(tankolasokSzama);
    for(int i = 0; i < tankolasokSzama; i++)
    {
        cin >> tankolasok[i].tav >> tankolasok[i].menny;
    }

    int elozo, most;
    elozo = benzin;
    most = benzin - tankolasok[0].tav/100*fogyasztas + tankolasok[0].menny;

    int i = 1;
    while(i<tankolasokSzama && !(most < elozo))
    {
        int tav = tankolasok[i].tav - tankolasok[i-1].tav;
        elozo = most;
        most = most - tav/100*fogyasztas + tankolasok[i].menny;
        i++;
    }

    van = most<elozo;
    if(van)
    {
        ind = i; //ind = i-1+1
    }
    else
    {
        ind = 0;
    }

    cout << ind << endl;

    return 0;
}
