#include <iostream>
#include <vector>

using namespace std;

struct Foglalas
{
    int terem, elso, utolso;
};

int main()
{
    int termekSzama;
    int napokSzama;
    int foglalasokSzama;
    vector<Foglalas> foglalasok;
    int nap;

    cin >> termekSzama >> napokSzama >> foglalasokSzama;
    foglalasok.resize(foglalasokSzama);
    for(int i = 0; i < foglalasokSzama; i++)
    {
        cin >> foglalasok[i].terem >> foglalasok[i].elso >> foglalasok[i].utolso;
    }

    //napok vektor deklaralasa: napokSzama kezdomeret, 0 kezdoertek minden elemnek
    vector<int> napok(napokSzama, 0);

    //Szamlalo tomb technika
    for(int i = 0; i < foglalasokSzama; i++)
    {
        for(int j = foglalasok[i].elso; j <= foglalasok[i].utolso; j++)
        {
            napok[j-1]++;
        }
    }

    //Maximum-kivalasztas
    nap = 0;
    for(int i = 1; i < napokSzama; i++)
    {
        if(napok[i] > napok[nap])
        {
            nap = i;
        }
    }

    cout << nap+1 << endl;

    return 0;
}
