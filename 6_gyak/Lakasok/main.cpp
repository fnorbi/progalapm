#include <iostream>
#include <vector>

using namespace std;

struct Lakas
{
    int ar;
    int meret;
};

int main()
{
    int n;
    vector<Lakas> lakasok;
    int meret, ar;
    bool van;
    int ind;

    cin >> n >> meret >> ar;
    lakasok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> lakasok[i].ar >> lakasok[i].meret;
    }

    int i = 0;
    while(i<n && !(lakasok[i].meret > meret && lakasok[i].ar > ar))
    {
        i++;
    }

    van = i<n;
    /*
    if(van)
    {
        ind = i+1;
    }
    else
    {
        ind = 0;
    }
    */
    ind = van ? i+1 : 0; //ternary operator

    cout << ind << endl;

    return 0;
}
