#include <iostream>

using namespace std;

int main()
{
    //Delaracio
    int o, p, mp;
    int t;

    //Beolvasas
    cout << "Add meg az orat: ";
    cin >> o;
    cout << "Add meg a percet: ";
    cin >> p;
    cout << "Add meg a masodpercet: ";
    cin >> mp;

    //Feldolgozas
    t = 60*60*o + 60*p + mp;

    //Kiiras
    cout << "A nap kezdete ota " << t << " masodperc telt el." << endl;

    return 0;
}
