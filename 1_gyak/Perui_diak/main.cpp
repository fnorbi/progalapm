#include <iostream>

using namespace std;

int main()
{
    //Deklaracio
    int jegy;
    bool bukott;

    //Beolvasas
    cout << "Add meg a jegyet: ";
    cin >> jegy;
    bool jo = 1 <= jegy && jegy <= 20;
    if(!jo)
    {
        cout << "Nem jo erteket adtal meg [1<=jegy<=20]." << endl;
        exit(1);
    }

    //Feldolgozas
    bukott = jegy <= 10;

    //Kiiras
    if(bukott)
    {
        cout << "bukott" << endl;
    }
    else
    {
        cout << "nem bukott" << endl;
    }

    return 0;
}
