#include<iostream>

using namespace std;

int main()
{
    // Ez itt egy komment
    /*
    string s1 = "Programozasi";
    string s2 = "alapismeretek";
    string s = s1 + " " + s2;
    cout << s << endl;
    */

    //Aritmetika: +, -, *, /, %
    cout << 1 + 2 << endl;
    cout << 14 / 5 << endl; // div
    cout << 14 % 5 << endl; // mod
    cout << 14.0 / 5 << endl;
    cout << (double)14 / 5 << endl;

    //Relaciok: <, <=, >, >=, ==, !=
    bool b = 1 > 0; // ertekadas algoban :=, kodban =
    cout << (1 > 0) << endl;
    cout << (b == true) << endl; // egyenloseg vizsgalat algoban: =, kodban: ==
    cout << (1 != 2) << endl; // nem egyenlo

    //Logikai: es: &&, vagy: ||
    bool logikai1 = 1 < 7 && 7 < 3;
    //bool logikai2 = 1 < 7 < 3;
    cout << logikai1 << endl;
    //cout << logikai2 << endl;

    //Tipusok: int (egesz), bool (logikai), char (karakter), double (valos)
    double d1 = 0.1;
    double d2 = 0.2;
    double d3 = 0.3;
    cout << (d1 + d2 == d3) << endl;
    cout << (0.1 + 0.2 == 0.3) << endl;

    int n;
    cout << "Adj meg egy szamot: ";
    cin >> n;
    cout << "A beolvasott ertek: " << n << endl;

    return 0;
}
