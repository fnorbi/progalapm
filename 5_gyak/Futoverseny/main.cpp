#include <iostream>
#include <vector>

using namespace std;

struct Meres
{
    int ind, erk;
};

int main()
{
    int n;
    vector<Meres> meresek;
    int minindex;
    int minido;
    bool holtverseny;

    cin >> n;
    meresek.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> meresek[i].ind >> meresek[i].erk;
    }

    /*
    //Minimum-kivalasztas tetel
    minindex = 0;
    minido = meresek[0].erk - meresek[0].ind;
    for(int i = 1; i < n; i++)
    {
        if(meresek[i].erk - meresek[i].ind < minido)
        {
            minindex = i;
            minido = meresek[i].erk - meresek[i].ind;
        }
    }

    cout << "A leggyorsabb futo sorszama: " << minindex + 1 << endl;
    cout << "A leggyorsabb futo ideje: " << minido << endl;

    //Eldontes tetel
    int i = 0;
    while(i<n && !(i != minindex && meresek[i].erk - meresek[i].ind == minido))
    {
        i++;
    }
    holtverseny = i<n;

    if(holtverseny)
    {
        cout << "Volt holtverseny." << endl;
    }
    else
    {
        cout << "Nem volt holtverseny." << endl;
    }
    */

    minindex = 0;
    minido = meresek[0].erk - meresek[0].ind;
    int db = 1;
    for(int i = 1; i < n; i++)
    {
        int aktido = meresek[i].erk - meresek[i].ind;
        if(aktido == minido)
        {
            db++;
        }
        else if(aktido < minido)
        {
            minindex = i;
            minido = aktido;
            db = 1;
        }
    }
    holtverseny = db>1;

    cout << "A leggyorsabb futo sorszama: " << minindex + 1 << endl;
    cout << "A leggyorsabb futo ideje: " << minido << endl;
    if(holtverseny)
    {
        cout << "Volt holtverseny. A minimalis idot futok szama: " << db << endl;
    }
    else
    {
        cout << "Nem volt holtverseny." << endl;
    }

    return 0;
}
