#include <iostream>
#include <vector>

using namespace std;

struct Pont
{
    double x, y;
};

int main()
{
    int n;
    double u, v, r;
    vector<Pont> pontok;
    vector<int> belsopontok;

    //Beolvasas

    cin >> n >> r >> u >> v;
    pontok.resize(n);
    for(int i = 0; i < n; i++)
    {
        cin >> pontok[i].x >> pontok[i].y;
    }

    //Feldolgozas = megszamolas tetel + kivalogatas tetel

    int db = 0;
    for(int i = 0; i < n; i++)
    {
        Pont p = pontok[i];
        if((p.x-u)*(p.x-u) + (p.y-v)*(p.y-v) < r * r)
        {
            db++;
            belsopontok.push_back(i);
        }
    }

    //Kiiras
    cout << db << " darab pont esik a kor belsejebe!" << endl;
    cout << "A belso pontok sorszamai: " << endl;
    for(int i = 0; i < db; i++)
    {
        cout << belsopontok[i] + 1 << " ";
    }
    cout << endl << "A belso pontok koordinatai: " << endl;
    for(int i = 0; i < db; i++)
    {
        //cout << pontok[belsopontok[i]].x << " " << pontok[belsopontok[i]].y << endl;
        int ind = belsopontok[i];
        cout << pontok[ind].x << " " << pontok[ind].y << endl;
    }

    return 0;
}
